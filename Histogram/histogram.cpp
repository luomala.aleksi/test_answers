#pragma once
#include "histogram_base.h"
#include "histogram.h"

#include <iostream>

Hist::Histogram::Histogram()
    :HistogramBase(std::make_unique<Logger>()),
    m_maxValue( Hist::EInteger::Zero ),
    m_minValue( Hist::EInteger::Zero ),
    m_mode( { Hist::EInteger::Zero, 0} )
{
}

Hist::Histogram::Histogram( std::unique_ptr<Logger> logger )
    : HistogramBase(std::move(logger)),
    m_maxValue( Hist::EInteger::Zero ),
    m_minValue( Hist::EInteger::Zero ),
    m_mode( { Hist::EInteger::Zero, 0 } )
{
}

Hist::Histogram::~Histogram()
{
}

void Hist::Histogram::add( EInteger value )
{
    auto search = m_values.find( value );
    if ( search != m_values.end() )
    {
        search->second++;
        _update_mode( search->first );
    }
    else
    {
        m_values.insert( { value, 1 } );
        _update_max_value( value );
        _update_min_value( value );
        _update_mode( value );
    }
}

Hist::EInteger Hist::Histogram::getMode() const
{
    return m_mode.first;
}

Hist::EInteger Hist::Histogram::getMinValue() const
{
    return m_minValue;
}

Hist::EInteger Hist::Histogram::getMaxValue() const
{
    return m_maxValue;
}

void Hist::Histogram::_update_mode( const EInteger key )
{
    //this only updates mode if it's greates, if it's equal amount to the previous, the previous is left
    value_count count = m_values.at( key );
    if ( count > m_mode.second )
    {
        m_mode.first = key;
        m_mode.second = count;
    }
}

void Hist::Histogram::_update_max_value( const EInteger value )
{
    if ( m_maxValue < value )
    {
        m_maxValue = value;
    }
}

void Hist::Histogram::_update_min_value( const EInteger value )
{
    if ( m_minValue > value )
    {
        m_minValue = value;
    }
}

Hist::RandomEintegerGenerator::RandomEintegerGenerator()
    :m_mt( ( std::random_device() )( ) ),
    dist( Hist::EInteger::Zero, Hist::EInteger::Four )
{
    //A possible hack to make this future proof could be adding COUNT to EInteger enum as the last value.
    //This way we can use COUNT - 1 as the distribution value in case we want to add more integers into the enum's list.
    //something to consider. Currently it doesn't feel very 'future proof'.
}
