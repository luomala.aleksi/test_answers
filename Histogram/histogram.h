#pragma once
#include "histogram_base.h"
#include <vector>
#include <memory>
#include <unordered_map>
#include <random>

typedef int value_count;
namespace Hist
{
    class RandomEintegerGenerator : public RandomEintegerGeneratorBase
    {
    public:
        /**
        * @brief Provides randomly generated numbers based on the EInteger enum.
        *
        * Usage:
        * @code
        *   RandomEintegerGenerator rng;
        *   EInteger value = rng();
        * @endcode
        */
        RandomEintegerGenerator();
        // Returns a pseudo-random EInteger value
        EInteger operator()() override
        {
            return static_cast<EInteger>( dist( m_mt ) );
        }
    private:
        std::mt19937 m_mt;
        std::uniform_int_distribution<int> dist;
    };

    /**
    * @brief Histogram provides functionality for the base class implementation.
    * Min, max values and mode can be requested from the data that is provided to
    * the Histogram.
    * 
    * Usage:
    * @code
    *   Histogram gram;
    *   gram.add( Hist::EInteger::One );
    *   gram.add( Hist::EInteger::One );
    *   gram.add( Hist::EInteger::Two );
    *   std::cout << gram.getMode() << " " << gram.getMinValue() << " " << gram.getMaxValue << "\n";
    * @endcode
    * 
    * Note:
    * getMode()
    *   m_mode only updates when a value that's inserted has a larger reference count than previous mode.
    *   If the new one has the same amount, it won't be regarded as the mode value. So if we first insert
    *   value 4 three times, then add three 6's, calling getMode() returns 4 as mode.
    */
    class Histogram : public HistogramBase
    {
    public:
        Histogram( ); //not quite sure if the logger is to be provided outside or not, so I made both constructors
        Histogram( std::unique_ptr<Logger> logger );
        ~Histogram() override;

        void add( EInteger value ) override;
        EInteger getMode() const override;
        EInteger getMinValue() const override;
        EInteger getMaxValue() const override;
    private:
        void _update_mode( const EInteger key );
        void _update_max_value(const EInteger value);
        void _update_min_value(const EInteger value);

        EInteger m_maxValue;
        EInteger m_minValue;
        std::pair<EInteger, value_count> m_mode;
        std::unordered_map<EInteger, value_count> m_values;
    };
}