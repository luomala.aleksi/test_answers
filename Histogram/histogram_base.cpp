#include "histogram_base.h"

Hist::HistogramBase::HistogramBase( std::unique_ptr<Logger> logger)
    :m_log(std::move(logger))
{
}

Hist::HistogramBase::HistogramBase( const HistogramBase& base)
    :m_log(  )
{
}

Hist::HistogramBase::HistogramBase( HistogramBase&& base )
    :m_log(std::move(base.m_log))
{
}

Hist::HistogramBase::~HistogramBase()
{
}
