#pragma once
#include <string>
class Logger
{
public:
    Logger();
    ~Logger();
    void log( const std::string& log );
};
