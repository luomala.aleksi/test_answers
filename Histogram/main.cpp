#include "histogram.h"
#include <iostream>
#include <array>
#include <algorithm>
#include <functional>

int main()
{
    Hist::Histogram histogram;
    Hist::RandomEintegerGenerator rng;
    const int test_value_count = 10;

    std::array<Hist::EInteger, test_value_count> test_values{};
    for ( size_t i = 0; i < test_value_count; i++ )
    {
        test_values[i] = rng();
        histogram.add( test_values[i] );
    }

    std::sort( test_values.begin(), test_values.end() );
    std::cout << "values:";
    for ( size_t i = 0; i < test_value_count; i++ )
    {
        std::cout << " " << test_values[i];
    }
    std::cout << "\n";

    std::cout << "max:" << histogram.getMaxValue() 
            << "  min:" << histogram.getMinValue()
            << "  mode:" << histogram.getMode()
            << "\n";
}